
const ProductFormVali = (validation,product) => {
  validation.Price = (product.Price !==undefined) && (product.Price !==0);
  validation.SKU =  (product.SKU !==undefined) && (product.SKU.length !== 0);
  validation.Name = (product.Name !==undefined) && (product.Name.length !==0);
  validation.Type = (product.Type !==undefined) && (product.Type !==0);


}
const DVDFormVali = (validation,product) => {

  validation.Size = (product.Size!== undefined) && (product.Size.length !== 0);

}
const BookFormVali = (validation,product) => {

  validation.Weight = (product.Weight !== undefined) && (product.Weight.length !== 0);

}
const FurnitureFormVali = (validation,product) => {

  validation.Height = (product.Height !== undefined) && (product.Height.length !== 0);
  validation.Width = (product.Width !== undefined) && (product.Width.length !== 0);
  validation.Length = (product.Length !== undefined) && (product.Length.length !== 0);

}

const FormValid = (validation, msg) => {
    let alertMsg =""
    Object.entries(validation).forEach(([key, value]) => {
      (value === false || value === undefined ) && (alertMsg=alertMsg+"\n *"+ key)});
       return alertMsg;

}



export { ProductFormVali, DVDFormVali, BookFormVali,  FurnitureFormVali, FormValid }
