import styled from 'styled-components';

export const ProductDiv =styled.div`
max-width:20rem;
background: #fff;
display: flex;
flex-direction: column;
justify-content: flex-start;
text-align: center;
border-radius: 1rem;
box-shadow: 0 0.1rem 0.3rem rgba(0,0,0,0.2);
transition: all 0.2s ease-in-out;

 & > span:first-of-type{
  align-self: start;
}
`

export const ProductContainer = styled.div`
max-width: 180rem;
margin: 0 auto;
display: grid;

grid-template-columns: repeat(auto-fit, minmax(15rem,1fr));
align-items: center;
grid-gap: 1rem;
padding: 2rem 5rem;
`



export const ProductCardH2 = styled.p`

font-size: 1.3rem;
margin-bottom: 0.05rem;
`



export const ProductCardDiv = styled.div`
padding: 0rem 2rem 1.5rem;
`
