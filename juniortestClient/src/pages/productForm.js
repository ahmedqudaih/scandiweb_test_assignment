import React from 'react';
import {ProductMainForm, FormOptionsCard} from '../components/styles/productFormStyle';
import {DropDownLists, FormInputs} from '../components/formInput';
import  { DVD, Book, Furniture } from '../components/productOptions';
import NavBar from '../components/navbar.js';
import {useNavigate} from 'react-router-dom';
import {getType, addProduct} from '../serverFunctions/productFunctions';
import  { ProductFormVali, DVDFormVali, BookFormVali, FurnitureFormVali, FormValid } from '../components/formValidation';

function ProductForm(){
  const navigate = useNavigate();
  const [product, setProduct] = React.useState({});
  const [productOptions, setProductOptions] = React.useState({});
  const [type, setType] = React.useState([]);

  React.useEffect(() => {
    const fetchData = async () => {
        const serverType = await getType();
        setType(serverType);
    }
    fetchData();
  }, []);

  let changeIn={
    "product": setProduct,
    "options": setProductOptions,
  }


  const option ={
      1: {obj:DVD,val:DVDFormVali},
      2: {obj:Book, val:BookFormVali},
      3: {obj:Furniture,val:FurnitureFormVali},
  };

  const ValidateProduct = () =>{

    let validation ={};
       ProductFormVali(validation , product);
        product.Type && option[product.Type].val(validation,productOptions);
       let subVali = FormValid(validation);
        subVali.length > 0 ? alert("Please provide the data for:" + subVali): submitProduct();
  }



  const submitProduct = async () =>{
    const formData = new FormData();
    formData.append("formData",JSON.stringify({"product":product,"options":productOptions}));

    await addProduct(formData);
    navigate('/');
  }

  function handleChange(event,changing) {
    let name = event.target.name;
     let value= event.target.value;
    changeIn[changing]((pre) => {
     return {
        ...pre,
          [name]:value
      };
    });
  }

  function handleDropDownListChange(event){
    setProductOptions({});
    handleChange(event,"product");
  }
console.log(product);
  return (
    <>
    <NavBar title="Product Add" submitProduct={ValidateProduct} />
        <ProductMainForm name="myform" id="product_form">
            <label htmlFor="SKU">SKU :</label>
            <FormInputs ID={"sku"} type={"text"} name={"SKU"} label={"SKU"} helperText={"Please Enter Product SKU"} handleChange={(event)=>{handleChange(event,"product")}} value={product.SKU||''}/>
            <label htmlFor="Name">Name :</label>
            <FormInputs ID={"name"} type={"text"} name={"Name"} label={"Name"} helperText={"Please Enter Product Name"} handleChange={(event)=>{handleChange(event,"product")}} value={product.Name||''}/>
            <label htmlFor="Price">Price($) :</label>
            <FormInputs ID={"price"} type={"number"} name={"Price"} label={"Price"} helperText={"Please Enter Product Price"} handleChange={(event)=>{handleChange(event,"product")}} value={product.Price||''}/>
            <label htmlFor="Type">Type :</label>
            <DropDownLists ID={"productType"} name={"Type"} handleChange={handleDropDownListChange} helperText={"Please select Product type"}  value={product.Type||''} options={type}/>
      </ProductMainForm >
      { product.Type!==undefined &&
        <FormOptionsCard key={product.Type}>
            {option[product.Type].obj({handleChange: handleChange,product: productOptions})}
        </FormOptionsCard >
      }
   </>
  );

}


export default ProductForm;
