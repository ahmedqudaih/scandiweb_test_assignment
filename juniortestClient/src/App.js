import './App.css';
import {BrowserRouter  as Router, Route, Routes} from 'react-router-dom';
import Footer from './components/footer.js';
import ProductForm from './pages/productForm.js';
import Main from './pages/main.js';
function App() {
  return (<>
    <Router>
      <Routes >
        <Route path="/" element={<Main />}/>
        <Route path="/addProduct" element={<ProductForm />}/>
      </Routes >
      <Footer/>
    </Router>
    </>
  );
}

export default App;
