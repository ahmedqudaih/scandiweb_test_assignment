import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Nav = styled.nav`
align-items: center;
font-size: 1.3rem;
`
export const NavLink = styled(Link)`
 text-decoration: none;
`
export const NavList = styled.ul`
list-style-type: none;
margin: 0;
padding: 0;
overflow: hidden;
`

export const Navitem = styled.li `
display: inline;
float:right;
padding: 0 1rem;
`
export const NavName = styled.li `
display: inline;
font-size: 2rem;
font-weight: bold;
text-decoration: none;
`
