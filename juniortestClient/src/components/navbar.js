import React from 'react';
import {Nav, NavList, Navitem, NavName, NavLink as Link} from './styles/navbarStyle'
import { useLocation } from 'react-router-dom';
import Button from '@mui/material/Button';
function Navbar(props){
  const { pathname } = useLocation();
  const formOptions= ()=>{
    return(<>
    <Navitem><Link to="/"><Button color={'error'} size={'small'} variant="contained">Cancel</Button></Link></Navitem>
      <Navitem onClick={props.submitProduct}><Button size={'small'} variant="contained">Save</Button></Navitem>
    </>);
  }
  const mainOptions = ()=>{
    return(<>
      <Navitem onClick={props.delete}><Button color={'error'} size={'small'} variant="contained">MASS DELETE</Button></Navitem>
      <Navitem><Link to="/addProduct"><Button size={'small'} variant="contained">ADD</Button></Link></Navitem>

    </>);
  }
  const options = {
    "/addProduct" : formOptions(),
    "/":mainOptions()
  }

  return (
    <Nav id={pathname}>
      <NavList>
      <NavName>{props.title}</NavName>
      {options[pathname]}
      </NavList>
      <hr className="solid" />
    </Nav>
  );

}


export default Navbar;
