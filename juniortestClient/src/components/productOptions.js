import React from "react"
import {FormInputs} from '../components/formInput';
import {ProductCardH2} from './styles/productStyle'

const DVD = (props) =>{
  return(
<>
<label htmlFor="Size">Size(MB) :</label>
<FormInputs ID={"size"} type={"text"} name={"Size"} label={"Size"} helperText={"Please Enter Product Size"} handleChange={(event)=>{props.handleChange(event,"options")}} value={props.product.Size||''}/>
</>
  );
}

const DVDView = (props) =>{
  return(
  <ProductCardH2>Size: {props.Size} MB</ProductCardH2>
  );
}


const Book = (props) =>{
  return(
<>
<label htmlFor="Weight">Weight(Kg) :</label>
<FormInputs ID={"weight"} type={"text"} name={"Weight"} label={"Weight"} helperText={"Please Enter Product Weight"} handleChange={(event)=>{props.handleChange(event,"options")}} value={props.product.Weight||''}/>

</>
  );
}

const BookView = (props) =>{
  return(
    <ProductCardH2>Weight: {props.Weight} KG</ProductCardH2>
  );
}



const Furniture = (props) =>{
  return(
<>
<label htmlFor="Height">Height :</label>
<FormInputs ID={"height"} type={"text"} name={"Height"} label={"Height"} helperText={"Please Enter Product Height"} handleChange={(event)=>{props.handleChange(event,"options")}} value={props.product.Height||''}/>
<label htmlFor="Width">Width :</label>
<FormInputs ID={"width"} type={"text"} name={"Width"} label={"Width"} helperText={"Please Enter Product Width"} handleChange={(event)=>{props.handleChange(event,"options")}} value={props.product.Width||''}/>
<label htmlFor="Length">Length :</label>
<FormInputs ID={"length"} type={"text"} name={"Length"} label={"Length"} helperText={"Please Enter Product Length"} handleChange={(event)=>{props.handleChange(event,"options")}} value={props.product.Length||''}/>
</>
  )
}

const FurnitureView = (props) =>{
  return(
    <ProductCardH2>Dimensions: {props.Height}x{props.Width}x{props.Length}</ProductCardH2>
  )
}




export { DVD, Book, Furniture,  DVDView, BookView, FurnitureView }
