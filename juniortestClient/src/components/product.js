import React from 'react';
import {ProductDiv,ProductCardH2, ProductCardDiv} from './styles/productStyle'
import Checkbox from '@mui/material/Checkbox';
import  { DVDView, BookView, FurnitureView } from './productOptions';

function Product(props) {


  const option ={
      1: DVDView,
      2: BookView,
      3: FurnitureView,
  };
  return(

          <ProductDiv>
          <Checkbox
                checked={props.checked}
                className="delete-checkbox"
                onChange={()=>props.checkedList(props.data.SKU)}
                inputProps={{ 'aria-label': 'controlled' }}
              />
              <ProductCardDiv>
            <ProductCardH2>{props.data.SKU}</ProductCardH2>
    <ProductCardH2>{props.data.Name}</ProductCardH2>
      <ProductCardH2>{props.data.Price} $</ProductCardH2>
      {option[props.data.Type_id](props.data)}
        </ProductCardDiv >
          </ProductDiv>
  );
}
export default Product;
