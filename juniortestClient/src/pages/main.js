import React from 'react';
import NavBar from '../components/navbar.js';
import Product from '../components/product.js';
import {ProductContainer} from '../components/styles/productStyle';
import {getProducts, deleteProduct} from '../serverFunctions/productFunctions';

function Main(){

  const [data, setData] = React.useState([]);
  const [checkedList, setCheckedList] = React.useState([]);
  const sortData = (array) => {
    return (array.sort((a,b) => {
      let aSKU = a.SKU.toLowerCase(),
            bSKU = b.SKU.toLowerCase();
      return ((aSKU > bSKU) ? 1 : ((bSKU > aSKU) ? -1 : 0))
    }));
  }
  React.useEffect(() => {
    const fetchData = async () => {
        const serverData = await getProducts();
        serverData.length > 0 && setData(sortData(serverData));

    }
    fetchData();
  }, [checkedList.length]);


  const handleDelete = async () =>{
    const formData = new FormData();
    formData.append("id",JSON.stringify(checkedList));
    await deleteProduct(formData);
    setCheckedList([]);
  }

  const handleCheckBoxChange = (id)=>{
    setCheckedList((pre) => {
      return pre.includes(id)?[...pre.filter(e => e !== id)]:[...pre,id];
    });
  }


  return (<>
    <NavBar title="Product List" delete={handleDelete} />
    <ProductContainer>
      {data.map((e) => (
          <Product key={e.SKU} data={e} checkedList={handleCheckBoxChange} checked={checkedList.includes(e.SKU)} />
      ))}
    </ProductContainer>

  </>);

}


export default Main;
