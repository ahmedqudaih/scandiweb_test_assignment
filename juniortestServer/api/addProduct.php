<?php


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods');

include_once '../model/ProductBookModel.php';
include_once '../model/ProductDvdModel.php';
include_once '../model/ProductFurnitureModel.php';

$bookModel = function(){
    return new ProductBookModel();
};
$dvdModel = function(){
    return new ProductDvdModel();
};
$furnitureModel = function(){
    return new ProductFurnitureModel();
};

$type =[
    '1'=> $dvdModel,
    '2'=> $bookModel,
    '3' => $furnitureModel,
];


try {
    $formData = json_decode($_POST['formData'],true);
    $model = $type[$formData['product']['Type']]();
    $model->addProducts($formData['product'],$formData['options']);
    echo "Done";
} catch (\Throwable $th) {
    echo "error";
}

?>