import styled from 'styled-components';

export const ProductMainForm =styled.form`
display: grid;
grid-template-columns: 6% 40%;
align-items: center;
padding: 2% 2%;
grid-gap: 0.2rem;
`
export const FormOptionsCard = styled.div`
width: 50%;
display: grid;
grid-template-columns: 20% 60%;
padding: 2% 2%;
grid-gap: 0.2rem;
margin: 0 2%;
`
