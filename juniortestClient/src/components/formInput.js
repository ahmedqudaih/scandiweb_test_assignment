import React from "react"
import {TextField,  NativeSelect } from '@mui/material';

const DropDownLists = (props) =>{

  return(
   <NativeSelect onChange = {props.handleChange}  name={props.name} id={props.ID}>
        <option  value = ""> {props.label}</option>
     {props.options.map((option) => (
     <option key={option.Id} value = {option.Id}>{option.Type}</option>
   ))}
   </NativeSelect>
  );
}


const FormInputs = (props) =>{
  return(
    <
    TextField
    id={props.ID}
    type = {props.type}
    label = {props.label}
    variant = "standard"
    name={props.name}
    helperText = {props.helperText}
    required
    onChange = {props.handleChange}
    value = {props.value}
    / >
  );
}






export { DropDownLists, FormInputs }
