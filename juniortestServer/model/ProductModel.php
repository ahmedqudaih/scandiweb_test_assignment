<?php
include_once '../model/Db.php';
abstract class ProductModel{
    protected $db;
    private $table;
    private $arr = array();
    public function __construct(){
        $this->db = new Db();
    }
        
    private function getBooks()
    {
        $query="SELECT `SKU`,`Name`,`Price`, `Type_id` ,`Weight` FROM `product` INNER JOIN `book` ON product.SKU = book.Product_id WHERE EXISTS (SELECT * FROM `book` WHERE book.Product_id = product.SKU)";
        $result= $this->db->database_query($query);
        $this->arr=$this->db->database_result_assoc($result, $this->arr);
    }
    private function getDvd()
    {
        $query="SELECT `SKU`,`Name`,`Price` , `Type_id`,`Size` FROM `product` INNER JOIN `dvd` ON product.SKU = dvd.Product_id WHERE EXISTS (SELECT * FROM `dvd` WHERE dvd.Product_id = product.SKU)";
        $result= $this->db->database_query($query);
        $this->arr=$this->db->database_result_assoc($result, $this->arr);
    }
    private function getFurniture()
    {
        $query="SELECT `SKU`,`Name`,`Price`, `Type_id`, `Height` ,`Width`, `Length` FROM `product` INNER JOIN `furniture` ON product.SKU = furniture.Product_id WHERE EXISTS (SELECT * FROM `furniture` WHERE furniture.Product_id = product.SKU)";  
        $result= $this->db->database_query($query);
        $this->arr =$this->db->database_result_assoc($result, $this->arr);
    }

    public function getAllProducts(){
        $this->getBooks();
        $this->getDvd();
        $this->getFurniture();
        return json_encode($this->arr);
    }

    public function addProducts($formData, $options){
        try {
            $query="INSERT INTO `product` (`SKU`, `Name`, `Price`, `Type_id`) VALUES ('$formData[SKU]', '$formData[Name]' , '$formData[Price]','$formData[Type]')";
            $result= $this->db->database_query($query);
            $this->handelSpecificAttribute($options,$formData["SKU"]);
        } catch (\Throwable $th) {
            echo "error";
        }
        
    }
    public function deleteProducts($id){
        $query="DELETE from `product` Where `SKU` = $id ";
        $result= $this->db->database_query($query);
    }
    public function ProductsType(){
        $query="SELECT * FROM `type`";
        $result= $this->db->database_query($query);
        $this->arr = $this->db->database_result_assoc($result,$this->arr);
        return json_encode($this->arr);
    }
    abstract public function handelSpecificAttribute($formData,$SKU);


}

?>