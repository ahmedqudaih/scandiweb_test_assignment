const url = "https://scandiwebtest-ahmedqudaih-serv.herokuapp.com/api/";


exports.getProducts = function() {
  const requestOptions = {
     method: 'get',
 };
  return callServer(url+"getProduct", requestOptions );
}

exports.deleteProduct = function(idList) {
  const requestOptions = {
     method: 'POST',
     body: idList
 };
 return callServer(url+"deleteProduct", requestOptions );
  }

  exports.getType = function() {
    const requestOptions = {
       method: 'get',
   };
   return callServer(url+"getType", requestOptions );
  }

  exports.addProduct = function(formData) {
    const requestOptions = {
           method: 'POST',
           body: formData
       };

      return callServer(url+"addProduct", requestOptions );
  }

function callServer(url, requestOptions ){
    return (fetch(url+".php",requestOptions).then(response => {
     return response.json();
    }).then(data => {
      return data;
    }).catch(error => {
      //console.error("Error fetching data: ", error);
      return error;
    }));
    }
