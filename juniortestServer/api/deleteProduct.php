<?php


header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods');

include_once '../model/ProductBookModel.php';

$x = new ProductBookModel();

try {
    $id = json_decode($_POST['id'],true);
   
    foreach ($id as $key) {
        $x->deleteProducts(json_encode($key));;
    }
    echo "Done";
} catch (\Throwable $th) {
    echo "error";
}

?>